(function() {
    'use strict';

    var module = angular.module('app.panelController', []);

    module.controller('panelController', panelController);

    function panelController(mdPanelRef) {
        var vm = this;

        vm.mdPanelRef = mdPanelRef;

        vm.closeDialog = function() {
            vm.mdPanelRef && vm.mdPanelRef.close().then(function() {
                vm.mdPanelRef.destroy();
            });
        }
    }

}())

