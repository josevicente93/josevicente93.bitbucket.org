angular.module("app").config(RouteConfig);

RouteConfig.$inject = [ '$routeProvider' ];

function RouteConfig($routeProvider) {
	$routeProvider.when('/', { templateUrl : "./templates/spanish.html" });
    $routeProvider.when('/spanish', { templateUrl : "./templates/spanish.html" });
    $routeProvider.when('/espanol', { templateUrl : "./templates/spanish.html" });
    $routeProvider.when('/🇪🇸', { templateUrl : "./templates/spanish.html" });
    $routeProvider.when('/🐂', { templateUrl : "./templates/spanish.html" });
	$routeProvider.when('/english', { templateUrl : "./templates/english.html" });
	$routeProvider.when('/🇬🇧', { templateUrl : "./templates/english.html" });
    $routeProvider.when('/💂', { templateUrl : "./templates/english.html" });
    $routeProvider.otherwise({redirectTo: '/'});
}