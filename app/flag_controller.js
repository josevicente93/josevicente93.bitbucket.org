(function() {
    'use strict';

    var module = angular.module('app.flagController', []);

    module.controller('flagController', flagController);

    flagController.$inject = ['$route', '$location'];

    function flagController($route, $location) {
        var vm = this;

        var urlsList = $route.routes;
        var currentUrl = $location.path();

        vm.toEnglish = function() {
            $("#spanish").removeClass("chosen-language");
            $("#english").addClass("chosen-language");
            console.log("to English");
            $location.path("/english");
        }

        vm.toSpanish = function() {
            $("#english").removeClass("chosen-language");
            $("#spanish").addClass("chosen-language");
            console.log("to Spanish");
            $location.path("/espanol");
        }

        if (isAEnglishUrl(urlsList, currentUrl)) {
            vm.toEnglish();
        } else {
            if (isASpanishUrl(urlsList, currentUrl)) {
                vm.toSpanish();
            }
        }

    }

    function isAEnglishUrl(urlsList, currentUrl) {
        return !isASpanishUrl(urlsList, currentUrl);
    }

    function isASpanishUrl(urlsList, currentUrl) {
        var spanishUrls = [];

        for (var url in urlsList) {
            var stringUrl = url.toString();

            if (stringUrl != "null" && stringUrl != "") {
                spanishUrls.push(url.toString());
            }
        }

        spanishUrls = spanishUrls.slice(0, 8);

        return spanishUrls.indexOf(currentUrl) != -1;
    }

}())

