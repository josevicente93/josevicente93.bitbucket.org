(function() {
    'use strict';

    var module = angular.module('app.controller', ['ngMaterial']);

    module.controller('cvController', cvController);

    cvController.$inject = ['$mdPanel'];

    function cvController($mdPanel) {
        var vm = this;

        vm.mdPanel = $mdPanel;

        var position = $mdPanel.newPanelPosition()
            .absolute()
            .center();

        var panelAnimation = $mdPanel.newPanelAnimation()
            .withAnimation($mdPanel.animation.FADE)
            .closeTo({
                top: document.documentElement.clientHeight,
                left: document.documentElement.clientWidth / 2 - 250
            });

        vm.config = {
            animation: panelAnimation,
            attachTo: angular.element(document.body),
            controller: 'panelController',
            controllerAs: 'ctrl',
            hasBackdrop: true,
            panelClass: 'software-engineering-details',
            position: position,
            trapFocus: true,
            zIndex: 150,
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: true
        };

        vm.showSoftwareEngineeringDialogEn = function() {
            vm.config.templateUrl = './app/templates/softwareEngineeringDetails.html';

            openPanel();
        }

        vm.showSoftwareEngineeringDialogEs = function() {
            vm.config.templateUrl = './app/templates/softwareEngineeringDetailsEs.html';

            openPanel();
        }

        function openPanel() {
            vm.mdPanel.open(vm.config)
                .then(
                    function(response) {
                        vm.panel = response;
                    });
        }
    }

}())
